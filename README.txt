CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This Adobe DTM module allows the use of Adobe Systems' Dynamic Tag Manager
hosted application to insert new tags into your Drupal website.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/adobe_dtm

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/adobe_dtm


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/documentation/install/modules-themes/modules-8
   for further information.

 * Set permissions for the Adobe DTM module, to define who can administer
   the configuration options of the module.


CONFIGURATION
-------------

Go to the configuration for of the Adobe DTM module (/admin/config/system/adobe-dtm).
Configure the embed code ID & hash and which Adobe DTM environment needs to be used.


MAINTAINERS
-----------

Current maintainers:
 * Jochen Verdeyen (jover) - https://drupal.org/user/310720
