<?php

namespace Drupal\adobe_dtm\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class for the Adobe Dynamic Tag Management settings.
 */
class AdobeDynamicTagManagementSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'adobe_dtm_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['adobe_dtm.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('adobe_dtm.settings');

    $form['settings'] = [
      '#type'       => 'vertical_tabs',
      '#attributes' => ['class' => ['adobe-dtm-settings']],
    ];

    // General section.
    $form['general'] = [
      '#type'  => 'details',
      '#title' => $this->t('General'),
      '#group' => 'settings',
    ];

    $form['general']['connection_type'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Connection type'),
      '#description'   => $this->t('Whether the script injection is like Adobe DTM or Adobe Launch.'),
      '#options'       => [
        ADOBE_DTM_CONNECTION_TYPE_DTM    => 'DTM',
        ADOBE_DTM_CONNECTION_TYPE_LAUNCH => 'Launch',
      ],
      '#default_value' => $config->get('connection_type'),
    ];

    $form['general']['enabled'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enable Adobe Tag Management'),
      '#description'   => $this->t('Will enable the Adobe Tag Management script, either using DTM or Launch as configured above.'),
      '#default_value' => $config->get('enabled'),
    ];

    $form['general']['property_settings'] = [
      '#type'   => 'fieldset',
      '#title'  => $this->t('Property settings'),
      '#states' => [
        'visible' => [
          ':input[name="enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['general']['property_settings']['dtm_property_embed_code_id'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('ID'),
      '#description'   => $this->t('DTM embed code ID.'),
      '#default_value' => $config->get('dtm_property_embed_code_id'),
      '#size'          => 40,
      '#maxlength'     => 150,
      '#states'        => [
        'visible' => [
          ':input[name="connection_type"]' => ['value' => ADOBE_DTM_CONNECTION_TYPE_DTM],
        ],
        'required' => [
          ':input[name="connection_type"]' => ['value' => ADOBE_DTM_CONNECTION_TYPE_DTM],
        ],
      ],
    ];

    $form['general']['property_settings']['dtm_property_embed_code_hash'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Hash'),
      '#description'   => $this->t('DTM embed code hash.'),
      '#default_value' => $config->get('dtm_property_embed_code_hash'),
      '#size'          => 40,
      '#maxlength'     => 150,
      '#states'        => [
        'visible' => [
          ':input[name="connection_type"]' => ['value' => ADOBE_DTM_CONNECTION_TYPE_DTM],
        ],
        'required' => [
          ':input[name="connection_type"]' => ['value' => ADOBE_DTM_CONNECTION_TYPE_DTM],
        ],
      ],
    ];

    $form['general']['property_settings']['launch_property_build_url'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Build URL'),
      '#description'   => $this->t('Launch property build URL.'),
      '#default_value' => $config->get('launch_property_build_url'),
      '#size'          => 80,
      '#maxlength'     => 250,
      '#states'        => [
        'visible' => [
          ':input[name="connection_type"]' => ['value' => ADOBE_DTM_CONNECTION_TYPE_LAUNCH],
        ],
        'required' => [
          ':input[name="connection_type"]' => ['value' => ADOBE_DTM_CONNECTION_TYPE_LAUNCH],
        ],
      ],
    ];

    $form['general']['property_settings']['launch_property_async'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Load Library Asynchronously'),
      '#default_value' => $config->get('launch_property_async'),
      '#states'        => [
        'visible' => [
          ':input[name="connection_type"]' => ['value' => ADOBE_DTM_CONNECTION_TYPE_LAUNCH],
        ],
      ],
    ];

    // Paths section.
    $form['paths'] = [
      '#type'  => 'details',
      '#title' => $this->t('Paths'),
      '#group' => 'settings',
    ];

    $form['paths']['paths'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Listed paths'),
      '#description'   => $this->t("Enter one path per line. The '*' character is a wildcard. An example path is %node-edit-wildcard for every node edit page, %admin-wildcard targets administration pages.", [
        '%node-edit-wildcard' => '/node/*/edit',
        '%admin-wildcard'     => '/admin/*'
      ]),
      '#default_value' => $this->implodeListOfItems($config->get('paths')),
      '#rows'          => 10,
    ];

    $form['paths']['paths_negate'] = [
      '#type'          => 'radios',
      '#title_display' => 'invisible',
      '#default_value' => $config->get('paths_negate'),
      '#options'       => [
        ADOBE_DTM_PATHS_NEGATE_EXCLUDE => $this->t('Exclude on these paths'),
        ADOBE_DTM_PATHS_NEGATE_INCLUDE => $this->t('Include on these paths'),
      ],
    ];

    // Advanced section.
    $form['advanced'] = [
      '#type'  => 'details',
      '#title' => $this->t('Advanced'),
      '#group' => 'settings',
    ];

    $form['advanced']['datalayer_enabled'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enable Adobe Datalayer'),
      '#description'   => $this->t('Will enable the Adobe Tag Management datalayer output.'),
      '#default_value' => $config->get('datalayer_enabled'),
      '#states'        => [
        'visible' => [
          ':input[name="enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['advanced']['dtm_environment'] = [
      '#type'          => 'select',
      '#title'         => t('Environment'),
      '#options'       => array(
        'production' => t('Production'),
        'staging'    => t('Staging'),
      ),
      '#default_value' => $config->get('dtm_environment'),
      '#states'        => [
        'visible' => [
          ':input[name="connection_type"]' => ['value' => 'dtm'],
          ':input[name="enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Clean text values.
    $dtm_property_embed_code_id = trim($form_state->getValue('dtm_property_embed_code_id'));
    $form_state->setValue('dtm_property_embed_code_id', $dtm_property_embed_code_id);
    $dtm_property_embed_code_hash = trim($form_state->getValue('dtm_property_embed_code_hash'));
    $form_state->setValue('dtm_property_embed_code_hash', $dtm_property_embed_code_hash);
    $launch_property_build_url = trim($form_state->getValue('launch_property_build_url'));
    $form_state->setValue('launch_property_build_url', $launch_property_build_url);
    $paths = $this->splitAndCleanText($form_state->getValue('paths'));
    $form_state->setValue('paths', $paths);

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('adobe_dtm.settings')
      ->set('connection_type', $form_state->getValue('connection_type'))
      ->set('enabled', $form_state->getValue('enabled'))
      ->set('dtm_property_embed_code_id', $form_state->getValue('dtm_property_embed_code_id'))
      ->set('dtm_property_embed_code_hash', $form_state->getValue('dtm_property_embed_code_hash'))
      ->set('launch_property_build_url', $form_state->getValue('launch_property_build_url'))
      ->set('launch_property_async', $form_state->getValue('launch_property_async'))
      ->set('paths', $form_state->getValue('paths'))
      ->set('paths_negate', $form_state->getValue('paths_negate'))
      ->set('datalayer_enabled', $form_state->getValue('datalayer_enabled'))
      ->set('dtm_environment', $form_state->getValue('dtm_environment'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Splits new lines and cleans a string representing a list of items.
   *
   * @param string $text
   *   The string to clean.
   *
   * @return array
   *   The clean text array.
   */
  protected function splitAndCleanText($text) {
    $text = explode("\n", $text);
    $text = array_map('trim', $text);
    $text = array_filter($text, 'trim');
    return $text;
  }

  /**
   * Implodes a given list of items into a string.
   *
   * @param array $items
   *   The array with list of items.
   *
   * @return string
   *   The imploded list of items as a string.
   */
  protected function implodeListOfItems($items) {
    return implode("\n", $items);
  }

}
