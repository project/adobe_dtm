<?php

/**
 * @file
 * Hooks and documentation related to Adobe DTM module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the datalayer tags.
 *
 * @param array $datalayer_tags
 *   The datalayer tags array.
 * @param array $context
 *   An array with context.
 */
function hook_adobe_dtm_datalayer_tags_alter(array &$datalayer_tags, array &$context) {
}

/**
 * @} End of "addtogroup hooks".
 */
